#include "kernel.h"
#include "idt/idt.h"
uint16_t *video_mem = 0;
uint16_t terminal_row = 0;
uint16_t terminal_col = 0;
uint16_t terminal_make_char(char c, char color)
{
    return (color << 8) | c;
}

size_t strlen(const char * str)
{
    size_t len = 0;
    while (str[len])
    {
        len++;
    }
    return (len);
}

void    terminal_putchar(int x, int y, char c, char color)
{
    video_mem[(y * VGA_WIDTH) + x] = terminal_make_char(c, color);
}

void terminal_writechar(char c, char color)
{
    if (c == '\n')
    {
        terminal_row += 1;
        terminal_col = 0;
        return;
    }
    terminal_putchar(terminal_col, terminal_row, c, color);
    terminal_col += 1;
    if (terminal_col >= VGA_WIDTH)
    {
        terminal_col = 0;
        terminal_row += 1;
    }
}

void terminal_initialize()
{
    int y;
    int x;

    video_mem = (uint16_t*)(0xB8000);
    y = 0;
    x = 0;
    terminal_row = 0;
    terminal_col = 0;
    while(y < VGA_HEIGHT)
    {
        while(x < VGA_WIDTH)
        {
            terminal_putchar(x, y, ' ', 0);
            x++;
        }
        x = 0;
        y++;
    }
}

void putstr(const char *str)
{
    size_t len = strlen(str);
    int index = 0;
    while (index < len)
    {
        terminal_writechar(str[index], 15);
        index++;
    }
}


void kernel_main()
{
    terminal_initialize();   
    putstr("Welcome to Ngwane OS\nHello Ngwane !!\n");

    // Initialize the Interrupt Descriptor Table
    idt_init();
}