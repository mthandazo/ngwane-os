#include "memory.h"

void* memset(void* ptr, int c, size_t size)
{
    int index;

    char *c_ptr = (char *) ptr;
    index = 0;

    while(index < size)
    {
        c_ptr[index] = (char) c;
        index++;
    }
    return ptr;
}