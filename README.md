# Ngwane Operating System
Multithreaded Linux Kernel

## Toolbox
1. nasm
2. Qemu
3. dd
4. bless
5. gdb

## Compilation
./build.sh

## LOading the hda to the qemu
qemu-system-x86_64 -hda $bin_file

## Copying the bootloader to a usb stick
sudo dd -if=./boot.bin -fo=/dev/$usb_partition

## Attaching GDB to the QEMU emulator (First switch to protected mode)
gdb -q
> target remote | qemu-system-x86_64 -hda ./boot.bin -S -gdb stdio
> c
> layout asm
identify the jump forever

## Verifying if the kernel was loaded successfully with gdb
gdb -q
> add-symbol-file ./build/kernelfull.o 0x100000
> break _start
> target remote | qemu-system-x86_x64 -hda ./boot.bin -S -gdb stdio
> c
if the program hits the breakpoint, it means the kernel was loaded successfully.
