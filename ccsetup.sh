#!/bin/bash
echo "Installing cross compiler dependencies"
apt install build-essential -y
apt install bison -y
apt install flex -y
apt install libgmp3-dev -y
apt install libmpc-dev -y
apt install libmpfr-dev -y
apt install texinfo -y
apt install libcloog-isl-dev -y
apt install libisl-dev -y
